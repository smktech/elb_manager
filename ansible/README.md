# Ansible

> Follow the steps to bootstrap and provision ec2 instance, with elb_manager Rest API.

---

# 1. Create a env file ```.ansible-env```

The file ```.ansible-env``` is a simple solution for 2 porpouses:

1. segrate secret settings as aws access keys, path of private ssh key, in env variables;
2. load virtualenv and ssh key in current terminal (without ssh-agent);

Create the file from a sample:

```sh
cp .ansible-env.sample .ansible-env
vim .ansible-env
```

Update env with needed variables bellow:

```sh
#!/bin/bash
# more info about aws env vars:
# http://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html

export AWS_ACCESS_KEY_ID="CHANGE YOUR ACCESS KEY ID HERE !!!"
export AWS_SECRET_ACCESS_KEY="CHANGE YOUR SECRET ACCESS KEY ID HERE !!!"
export AWS_DEFAULT_REGION="us-east-2"
export AWS_PRIVATE_SSH_KEY="~/.ssh/elb_manager.pem"

eval ssh-add $AWS_PRIVATE_SSH_KEY
source .venv/bin/activate
```

# 2. Create a virtualenv

```sh
virtualenv -p python3 .venv
```

# 3. Load virtualenv with .ansible-env

```sh
source .ansible-env
```

# 4. Install ansible (in virtualenv)

```sh
pip install -r requirements.txt
```

# 5. Bootstrap ec2 instance

```sh
ansible-playbook -i ./hosts aws-resources.yml
```

# 6. Provision api instance

```sh
ansible-playbook -i ./hosts elb-manager.yml
```

> OBS: I splited out in 2 steps (Bootstrap) and (Provision), for speeds up productivity, avoiding managing AWS resources if it is not needed.
