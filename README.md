elb_manager
---

This project was executed with 3 main steps (separated by folders):

1. Development of Rest API, to manage AWS ELB resources

  **folder: elb_manager**

2. Development of configuration management, provisioning and automation of infrastructure as code

  **folder: ansible**

3. Stress test setup of the Rest API

  **folder: jmeter**
