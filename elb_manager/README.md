elb_manager
-----------

Running dev mode on your machine:

## 1.) Clone the project repo:

```
git clone https://bitbucket.org/smktech/elb_manager.git

cd elb_manager/elb_manage
```

## 2.) Creates the config file

```
sudo mkdir /etc/elb-manager
cp config/elb-manager/elb-manager.ini /etc/elb-manager
```

## 3.) Alter aws access key, secret key and aws region at ini file

```
vim /etc/elb-manager
```

Alter the peace of code like this
```
[aws]
aws_access_key_id = PUTYOURACCESSKEYHERE
aws_secret_access_key = PUTYOURSECRETACCESSKEYHERE
region_name = PUTYOURAWSREGIONHERE
```

## 4.) Creates a virtualenv:

```
virtualenv -p python3.6 .venv

source .venv/bin/activate

pip install -r requirements/common.txt
```

## 5.) Running the application (in dev mode)

```
./manage.py runserver
```

## 6.) Access in your browser the url of local service

[http://127.0.0.1:5000/](http://127.0.0.1:5000/)
