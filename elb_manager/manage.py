#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_script import Manager, Server
from elb_manager.app import create_app

app = create_app()
manager = Manager(app)

# command: runserver
runserver = Server(host=app.config['LISTEN_HOST'],
                   port=app.config['LISTEN_PORT'],
                   use_debugger=app.config['DEBUG'],
                   use_reloader=True)

manager.add_command("runserver", runserver)

# command: shell
@manager.shell
def make_shell_context():
    return {"app": app}

if __name__ == "__main__":
    manager.run()
