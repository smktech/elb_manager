#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from flask import current_app  # noqa
from elb_manager.app import create_app


@pytest.fixture(scope='session')
def app(request):
    """Create application context to execute tests."""

    test_conf = {'TESTING': True}
    app = create_app(config_overwrite=test_conf)
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app
