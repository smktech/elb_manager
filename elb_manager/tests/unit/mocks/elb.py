#!/usr/bin/env python
# -*- coding: utf-8 -*-

from botocore.exceptions import ClientError


class MockedElbClient():
    def __init__(self, elb_name=None, attached_instances=[],
                 error_code=None, fail_in_step=None):

        self.elb_name = elb_name
        self.stored_instances = {'Instances': attached_instances}
        self.exception = ClientError({'Error': {'Code': error_code}}, None)
        self.fail_in_step = fail_in_step

    def register_instances_with_load_balancer(self, *args, **kwargs):
        if self.fail_in_step == 'register':
            raise self.exception

        if kwargs['Instances']:
            self.stored_instances['Instances'] += kwargs['Instances']

        return self.stored_instances

    def deregister_instances_from_load_balancer(self, *args, **kwargs):
        if self.fail_in_step == 'deregister':
            raise self.exception

        if kwargs['Instances']:
            for instance_dict in kwargs['Instances']:
                self.stored_instances['Instances'].remove(instance_dict)

        return self.stored_instances

    def describe_load_balancers(self, *args, **kwargs):
        if self.fail_in_step == 'describe':
            raise self.exception
        else:
            return {'LoadBalancerDescriptions':
                        [{'LoadBalancerName': self.elb_name,
                          'Instances': self.stored_instances['Instances']}]}
