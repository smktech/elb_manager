#!/usr/bin/env python
# -*- coding: utf-8 -*-

from botocore.exceptions import ClientError


class MockedLauchTime():
    def isoformat(self):
        return "2016-08-29T09:12:33.001Z"


class MockedInstance():
    def __init__(self, instance_id, *args, **kwargs):
        self.id = instance_id
        self.instance_type = 't2.micro'
        self.launch_time = MockedLauchTime()
        self.state = None

        if kwargs.get('error_code'):
            raise ClientError({'Error': {'Code': kwargs['error_code']}}, None)


class MockedEC2Resourse():
    def __init__(self, error_code=None):
        self.error_code = error_code

    def Instance(self, instance_id, *args, **kwargs):
        return MockedInstance(
            instance_id, error_code=self.error_code, *args, **kwargs)
