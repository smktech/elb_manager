# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""List instances attached to a particular load balancer."""

from tests.unit.mocks.ec2 import MockedEC2Resourse
from tests.unit.mocks.elb import MockedElbClient


def test_successfully_instance_listing(client, mocker):
    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = MockedElbClient(
        elb_name='default-elb',
        attached_instances=[{'InstanceId': 'i-5203422c'},
                            {'InstanceId': 'other-instance-to-test-1'},
                            {'InstanceId': 'other-instance-to-test-2'}])

    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = MockedEC2Resourse()

    response = client.get('/elb/default-elb',
                          headers={'Content-Type': 'application/json'})
    attached_instances = response.json

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'

    assert len(attached_instances) == 3

    assert {'instanceId': 'i-5203422c',
            'instanceType': 't2.micro',
            'launchDate': '2016-08-29T09:12:33.001Z'} in attached_instances


def test_invalid_elb_to_list(client, mocker):
    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = MockedElbClient(error_code='LoadBalancerNotFound',
                                            fail_in_step='describe')

    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = None

    response = client.get('/elb/invalid-elb-to-list',
                          headers={'Content-Type': 'application/json'})

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the elb does not exist'}
