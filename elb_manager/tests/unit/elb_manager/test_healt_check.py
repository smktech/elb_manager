# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""API health check."""

def test_healt_check(client):
    response = client.get('/healthcheck')

    assert response.status_code == 200
    assert response.json == {'reason': 'the service is up'}
