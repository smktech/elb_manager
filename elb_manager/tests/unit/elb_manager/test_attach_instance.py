# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""Attach an instance on the load balancer."""

import json

from tests.unit.mocks.ec2 import MockedEC2Resourse
from tests.unit.mocks.elb import MockedElbClient


def test_successfully_instance_attachment(client, mocker):
    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = MockedElbClient(
        elb_name='default-elb', attached_instances=[])

    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = MockedEC2Resourse()

    response = client.post('/elb/default-elb',
                           data=json.dumps({'instanceId': 'i-5203422c'}),
                           headers={'Content-Type': 'application/json'})

    attached_instances = elb_mock.return_value.stored_instances['Instances']

    assert attached_instances == [{'InstanceId': 'i-5203422c'}]

    assert response.status_code == 201
    assert response.headers['Content-Type'] == 'application/json'
    assert len(response.json) == 1
    assert response.json == [{'instanceId': 'i-5203422c',
                              'instanceType': 't2.micro',
                              'launchDate': '2016-08-29T09:12:33.001Z'}]


def test_instance_is_already_attached_to_elb(client, mocker):
    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = MockedEC2Resourse()

    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = MockedElbClient(
        elb_name='default-elb',
        attached_instances=[{'InstanceId': 'i-5203422c'}])

    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': 'i-5203422c'}))

    attached_instances = elb_mock.return_value.stored_instances['Instances']

    assert attached_instances == [{'InstanceId': 'i-5203422c'}]

    assert response.status_code == 409
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'instance already on load balancer'}


def test_invalid_instance_to_attach_at_elb(client, mocker):
    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = MockedEC2Resourse(
        error_code='InvalidInstanceID.Malformed')

    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = None

    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': 'invalid-instance-do-attach'}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the instance does not exist'}


def test_invalid_elb_to_attach(client, mocker):
    ec2_mock = mocker.patch('elb_manager.api.list_instances.aws.resource')
    ec2_mock.return_value = MockedEC2Resourse()

    elb_mock = mocker.patch('elb_manager.api.list_instances.aws.client')
    elb_mock.return_value = MockedElbClient(
        error_code='LoadBalancerNotFound', fail_in_step='describe')

    response = client.post('/elb/invalid-elb-to-attach',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': 'i-5203422c'}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the elb does not exist'}


def test_wrong_data_format_for_instance_attachment_to_elb(client):
    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'wrong param': 'invalid value'}))

    assert response.status_code == 400
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'wrong data format'}
