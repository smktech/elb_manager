# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""Test all elb_manager operations."""

import json
import pytest


@pytest.mark.run(order=1)
def test_attach_instance_to_elb(client, ec2_instance):
    """Attach an instance on the load balancer."""

    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 201
    assert response.headers['Content-Type'] == 'application/json'
    assert ec2_instance in [i['instanceId'] for i in response.json]


@pytest.mark.run(order=2)
def test_ensure_instance_attachment_on_elb(client, ec2_instance):
    """List attached instances looking for a instance attached at step before."""

    response = client.get('/elb/default-elb',
                          headers={'Content-Type': 'application/json'})

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert ec2_instance in [i['instanceId'] for i in response.json]


@pytest.mark.run(order=3)
def test_instance_is_already_attached_to_elb(client, ec2_instance):
    """Attach an instance on the load balancer."""

    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 409
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'instance already on load balancer'}


@pytest.mark.run(order=4)
def test_detach_instance_from_elb(client, ec2_instance):
    """Detach an instance from the load balancer."""

    response = client.delete('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 201
    assert response.headers['Content-Type'] == 'application/json'
    assert ec2_instance in [i['instanceId'] for i in response.json]


@pytest.mark.run(order=5)
def test_ensure_instance_detachment_on_elb(client, ec2_instance):
    """List attached instances looking for a instance attached at step before."""

    response = client.get('/elb/default-elb',
                          headers={'Content-Type': 'application/json'})

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert ec2_instance not in [i['instanceId'] for i in response.json]


@pytest.mark.run(order=6)
def test_instance_is_already_detached_to_elb(client, ec2_instance):
    """Detach an instance from the load balancer."""

    response = client.delete('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 409
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'instance is not on load balancer'}


@pytest.mark.run(order=7)
def test_list_an_invalid_elb(client, ec2_instance):
    """List attached instances on invalid load balancer."""

    response = client.get('/elb/invalid-default-elb',
                          headers={'Content-Type': 'application/json'})

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the elb does not exist'}


@pytest.mark.run(order=8)
def test_attach_an_invalid_elb(client, ec2_instance):
    """Attach an instance on the invalid load balancer."""

    response = client.post('/elb/invalid-default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the elb does not exist'}


@pytest.mark.run(order=9)
def test_detach_an_invalid_elb(client, ec2_instance):
    """Detach an instance on the invalid load balancer."""

    response = client.delete('/elb/invalid-default-elb',
                             headers={'Content-Type': 'application/json'},
                             data=json.dumps({'instanceId': ec2_instance}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the elb does not exist'}


@pytest.mark.run(order=10)
def test_attach_invalid_instance_to_elb(client, ec2_instance):
    """Attach an invalid instance on the load balancer."""

    response = client.post('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': 'i-invalid-inst'}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the instance does not exist'}


@pytest.mark.run(order=11)
def test_detach_invalid_instance_to_elb(client, ec2_instance):
    """Detach an invalid instance on the load balancer."""

    response = client.delete('/elb/default-elb',
                           headers={'Content-Type': 'application/json'},
                           data=json.dumps({'instanceId': 'i-invalid-inst'}))

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'reason': 'the instance does not exist'}