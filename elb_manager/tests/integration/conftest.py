#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from flask import current_app  # noqa
from elb_manager.app import create_app

@pytest.fixture(scope='session')
def app(request):
    """Create application context to execute tests."""

    app = create_app()
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


def elb_client():
    from elb_manager.backend import aws
    return aws.client('elb')


@pytest.fixture(scope="module")
def ec2_instance(request):
    elb_api = elb_client()
    existing_instance = 'i-018af88a2b21f8ea0'

    elb_api.deregister_instances_from_load_balancer(
        LoadBalancerName='default-elb',
        Instances=[{'InstanceId': existing_instance}])

    # def teardown():
    #     elb_api.register_instances_with_load_balancer(
    #         LoadBalancerName='default-elb',
    #         Instances=[{'InstanceId': existing_instance}])
    #
    # request.addfinalizer(teardown)
    return existing_instance




