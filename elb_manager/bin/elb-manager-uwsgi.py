#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from urllib.parse import urlsplit

# Insert python local package to python path
sys.path.append(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '..')))

# create app object
from elb_manager.app import create_app
application = create_app()
app_context = application.app_context()
app_context.push()

if __name__ == "__main__":
    application.run(host=application.config['LISTEN_HOST'],
                  port=application.config['LISTEN_PORT'],
                  debug=application.config['DEBUG'])
