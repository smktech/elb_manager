#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from logging.handlers import RotatingFileHandler


def set_access_log(app):
    """Set elb-manager-access.log logging handler, level and format."""

    logger = logging.getLogger('werkzeug')

    accesslog_handler = RotatingFileHandler(
        app.config['ACCESS_LOGGING'],
        maxBytes=app.config['LOGGING_ROTATE_SIZE'],
        backupCount=app.config['LOGGING_ROTATE_COUNT'])

    logger.addHandler(accesslog_handler)


def set_error_log(app):
    """Set elb-manager-error.log logging handler, level and format."""

    errorlog_handler = RotatingFileHandler(
        app.config['ERROR_LOGGING'],
        maxBytes=app.config['LOGGING_ROTATE_SIZE'],
        backupCount=app.config['LOGGING_ROTATE_COUNT'])

    errorlog_handler.setLevel(app.config['LOGGING_LEVEL'])

    if 'LOGGING_FORMAT' in app.config:
        formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
        errorlog_handler.setFormatter(formatter)

    app.logger.addHandler(errorlog_handler)