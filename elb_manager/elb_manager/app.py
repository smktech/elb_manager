#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import yaml
import logging
from flask import Flask
from flasgger import Swagger

from elb_manager.logger import set_access_log, set_error_log

logger = logging.getLogger('elb_manager')


def register_api_endpoints(app):
    from elb_manager.api import elb_manager_api
    import elb_manager.api.swagger  # noqa
    import elb_manager.api.health_check  # noqa
    import elb_manager.api.list_instances  # noqa
    import elb_manager.api.attach_instance  # noqa
    import elb_manager.api.detach_instance  # noqa
    app.register_blueprint(elb_manager_api)


def create_app(config_overwrite={}):
    # build app object
    app = Flask(__name__)

    # load config
    app.config.from_object('elb_manager.settings.app.Configuration')
    app.config.update(config_overwrite)

    # set logger
    set_access_log(app)
    set_error_log(app)

    # register api urls
    register_api_endpoints(app)

    # swagger views
    swagger_path = os.path.join(app.config['BASE_DIR'],
                                'elb_manager/swagger/sre-test.yaml')
    with open(swagger_path) as swagger_file:
        swagger_template = yaml.safe_load(swagger_file)

    Swagger(app, template=swagger_template)

    return app
