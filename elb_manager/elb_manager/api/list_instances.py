#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify

from elb_manager.api import elb_manager_api
from elb_manager.backend import aws


@elb_manager_api.route('/elb/<elb_name>', methods=['GET'])
def list_instances(elb_name):
    elb_api = aws.client('elb')
    ec2_api = aws.resource('ec2')

    # get elb data
    try:
        elb_data = elb_api.describe_load_balancers(LoadBalancerNames=[elb_name])
    except aws.ClientError as exc:
        if exc.response['Error']['Code'] == 'LoadBalancerNotFound':
            payload = {'reason': 'the elb does not exist'}
            return jsonify(payload), 404
        else:
            # logger.error(boto_exception=repr(exc))
            payload = {'reason': 'failed at to get elb data'}
            return jsonify(payload), 500

    # get ec2 instances information
    payload = []
    for instance in elb_data['LoadBalancerDescriptions'][0]['Instances']:
        try:
            ec2_resource = ec2_api.Instance(instance['InstanceId'])

            payload.append({'instanceId': ec2_resource.id,
                            'instanceType': ec2_resource.instance_type,
                            'launchDate': ec2_resource.launch_time.isoformat()})

        except Exception as exc:
            payload = {'reason': 'failed at to get ec2 data from attached to instance'}
            return jsonify(payload), 500

    # return attached instances
    return jsonify(payload), 200
