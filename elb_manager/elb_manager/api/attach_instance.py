#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify, request

from elb_manager.api import elb_manager_api
from elb_manager.backend import aws
from elb_manager.decorators import validates_instance_payload


@elb_manager_api.route('/elb/<elb_name>', methods=['POST'])
@validates_instance_payload
def attach_instance(elb_name):
    instance_id = request.json_payload['instanceId']
    elb_api = aws.client('elb')
    ec2_api = aws.resource('ec2')

    # get ec2 instances information
    try:
        ec2_resource = ec2_api.Instance(instance_id)
        ec2_resource.state
    except Exception as exc:
        if exc.response['Error']['Code'] == 'InvalidInstanceID.Malformed':
            payload = {'reason': 'the instance does not exist'}
            return jsonify(payload), 404
        else:
            payload = {'reason': 'failed at to get ec2 instance data'}
            return jsonify(payload), 500

    # get existing elb instances
    try:
        elb_data = elb_api.describe_load_balancers(LoadBalancerNames=[elb_name])
    except aws.ClientError as exc:
        if exc.response['Error']['Code'] == 'LoadBalancerNotFound':
            payload = {'reason': 'the elb does not exist'}
            return jsonify(payload), 404
        else:
            payload = {'reason': 'failed at to get elb data'}
            return jsonify(payload), 500

    # check if instance is already attached
    attached_instances = elb_data['LoadBalancerDescriptions'][0]['Instances']
    if {'InstanceId': instance_id} in attached_instances:
        payload = {'reason': 'instance already on load balancer'}
        return jsonify(payload), 409

    # attach instance to elb
    try:
        attach_response = elb_api.register_instances_with_load_balancer(
            LoadBalancerName=elb_name,
            Instances=[{'InstanceId': instance_id}])
    except aws.ClientError as exc:
        payload = {'reason': 'failed at to attach instance'}
        return jsonify(payload), 500

    # check if instance was returned
    if {'InstanceId': instance_id} not in attach_response['Instances']:
        payload = {'reason': 'failed at to attach instance'}
        return jsonify(payload), 500

    # return attached instances
    payload = [{'instanceId': instance_id,
                'instanceType': ec2_resource.instance_type,
                'launchDate': ec2_resource.launch_time.isoformat()}]
    return jsonify(payload), 201
