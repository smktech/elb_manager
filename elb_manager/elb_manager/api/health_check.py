#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify

from elb_manager.api import elb_manager_api


@elb_manager_api.route('/healthcheck', methods=['GET'])
def health_check():
    return jsonify({'reason': 'the service is up'}), 200