#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import redirect

from elb_manager.api import elb_manager_api

@elb_manager_api.route('/')
def redirect_to_swagger():
    return redirect("/apidocs", code=302)


