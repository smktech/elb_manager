#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import configparser

config_parser = configparser.ConfigParser()

base_dir = os.path.join(
    os.path.abspath(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(__file__)))))

offical_config_file = '/etc/elb-manager/elb-manager.ini'
local_config_file = os.path.join(base_dir, 'config/elb-manager/elb-manager.ini')

check_offical_file = os.path.exists(offical_config_file)
check_local_file = os.path.exists(local_config_file)

if not check_offical_file and not check_local_file:
    raise Exception(
        'Config file not found: "{}".'.format(offical_config_file))

if check_offical_file:
    config_parser.read(offical_config_file)
else:
    config_parser.read(local_config_file)



