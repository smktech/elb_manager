#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from elb_manager.settings import config_parser, base_dir

class Configuration(object):
    BASE_DIR = base_dir

    # aws
    AWS_ACCESS_KEY_ID = config_parser.get('aws', 'aws_access_key_id')
    AWS_SECRET_ACCESS_KEY = config_parser.get('aws', 'aws_secret_access_key')
    REGION_NAME = config_parser.get('aws', 'region_name')

    # server
    LISTEN_HOST = config_parser.get('server', 'listen_host')
    LISTEN_PORT = config_parser.getint('server', 'listen_port')
    DEBUG = config_parser.getboolean('server', 'debug')

    # logging
    LOGGING_LEVEL = logging.DEBUG if DEBUG else logging.ERROR
    LOGGING_FOLDER = config_parser.get('logging', 'folder')
    LOGGING_ROTATE_SIZE = config_parser.getint('logging', 'rotate_size_in_mb') * 1024 * 1024
    LOGGING_ROTATE_COUNT = config_parser.get('logging', 'rotate_count')
    LOGGING_FORMAT = config_parser.get('logging', 'format', raw=True)
    ACCESS_LOGGING = os.path.join(LOGGING_FOLDER, 'elb-manager-error.log')
    ERROR_LOGGING = os.path.join(LOGGING_FOLDER, 'elb-manager-access.log')

