#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Documentation: http://boto.cloudhackers.com/en/latest/ref/elb.html """

import boto3
from botocore.exceptions import ClientError  # noqa

from flask import current_app


def client(scope):
    return boto3.client(scope,
                        aws_access_key_id=current_app.config['AWS_ACCESS_KEY_ID'],
                        aws_secret_access_key=current_app.config['AWS_SECRET_ACCESS_KEY'],
                        region_name=current_app.config['REGION_NAME'])


def resource(scope):
    return boto3.resource(scope,
                          aws_access_key_id=current_app.config['AWS_ACCESS_KEY_ID'],
                          aws_secret_access_key=current_app.config['AWS_SECRET_ACCESS_KEY'],
                          region_name=current_app.config['REGION_NAME'])
