#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
from flask import jsonify, request


def validates_instance_payload(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        request.json_payload = request.get_json(silent=True)
        if request.json_payload and 'instanceId' in request.json_payload.keys():
            return f(*args, **kwargs)
        else:
            return jsonify({'reason': 'wrong data format'}), 400

    return decorated
