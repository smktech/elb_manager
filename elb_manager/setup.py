#!/usr/bin/env python

import re
import os
from setuptools import find_packages, setup

VERSION_EXPR = re.compile(r'(\d+\.\d+\.\d+)')

with open('debian/changelog') as changelog_file:
    first_line = changelog_file.readline()
    match = VERSION_EXPR.search(first_line)
    if not match:
        raise Exception('Could not determine a valid version from of changelog file.')
    version = match.group(1)

setup(
    name='elb-manager',
    version=version,
    description='AWS ELB manager by REST API',
    author='Samuel Sampaio',
    author_email='samukasmk@gmail.com',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    scripts=[
        'bin/elb-manager-uwsgi.py',
        'manage.py'
    ],
)
