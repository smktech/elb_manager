# Stress tests

### Requirements:

- Java 8 (updated)
  http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html

### Install:

```
cd /opt
wget http://ftp.unicamp.br/pub/apache//jmeter/binaries/apache-jmeter-3.2.tgz
tar -zvxf apache-jmeter-3.2.tgz
```

### Run Stress test

```
/opt/apache-jmeter-3.2/bin/jmeter -n -t stress-test-elb-manager.jmx -l results-file.csv -j execution.log -e -o results
```
